package com.example.aaron.guestbook.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.aaron.guestbook.GuestBook.AddGuest;
import com.example.aaron.guestbook.GuestBook.ShowSearchResult;
import com.example.aaron.guestbook.R;
import com.example.aaron.guestbook.helper.AaronSearch;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.aaron.guestbook.Configure.HttpHint;
import static com.example.aaron.guestbook.Configure.URL_GUEST_HINT;
import static com.example.aaron.guestbook.Configure.URL_SEARCH_GUEST;

/**
 * Created by aaron on 3/21/2018.
 */

public class SearchFragmentForStaff extends Fragment{

    private AaronSearch searchView;
    private ProgressDialog pDialog;

    private final int RC_SEARCH = 1;
    private final int INTERVAL = 500;
    private Handler mHandler;
    private String key = "";
    private String type ="";




    @SuppressLint("HandlerLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_search_for_staff, container, false);
        setHasOptionsMenu(true);
        searchView = getActivity().findViewById(R.id.guest_searchView);
        RadioButton phone = view.findViewById(R.id.radio_phone);
        RadioButton name = view.findViewById(R.id.radio_name);
        RadioButton rating = view.findViewById(R.id.radio_rank);

        RadioGroup radioGroup = view.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radio_phone:
                        searchView.setHint("Search by phone");
                        type = "phone";
                        break;
                    case R.id.radio_name:
                        searchView.setHint("Search by name");
                        type = "name";
                        break;
                    case R.id.radio_rank:
                        searchView.setHint("Search by rank");
                        type = "rank";
                        break;
                }
            }
        });

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == RC_SEARCH) {
                    if(!type.equals("rank")){
                        if(!key.equals("")){
                            JSONObject data = new JSONObject();
                            try {
                                data.put("type", type);
                                data.put("key", key);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            HttpHint(getActivity(), URL_GUEST_HINT, data, searchView);
                        }
                  }
                }}
        };
        
        name.setChecked(true);
        type = "name";
        searchView.setHint("Search by name");
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(getActivity(), ShowSearchResult.class);
                intent.putExtra("type", type);
                intent.putExtra("key", key);
                startActivity(intent);
                searchView.closeSearch();
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                key = newText.trim();
                if (mHandler.hasMessages(RC_SEARCH)) {
                    mHandler.removeMessages(RC_SEARCH);
                }
                mHandler.sendEmptyMessageDelayed(RC_SEARCH, INTERVAL);
                return true;
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_bar, menu);
        searchView.setMenuItem(menu.findItem(R.id.action_search));
        super.onCreateOptionsMenu(menu, inflater);
    }
}