package com.example.aaron.guestbook.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aaron.guestbook.R;
import com.isapanah.awesomespinner.AwesomeSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static com.example.aaron.guestbook.Configure.HttpPost;
import static com.example.aaron.guestbook.Configure.URL_ADD_GUEST;

/**
 * Created by aaron on 3/13/2018.
 */

public class StaffFragment extends Fragment {

    private AwesomeSpinner meeting_type;
    private AwesomeSpinner first_time;
    private AwesomeSpinner payment_method;
    private AwesomeSpinner interviewer;
    private AwesomeSpinner contract_signed;

    private EditText quote_price;
    private EditText final_price;
    private EditText rating;
    private EditText remark;

    private CheckBox quote_box;
    private CheckBox final_box;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_staff, container, false);
        init(view);
        Button save = view.findViewById(R.id.btnSaveStaff);

        AwesomeSpinner.onSpinnerItemClickListener spinnerListener = new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int var1, String var2) {

            }
        };

        ArrayAdapter<CharSequence> meetingAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.meeting_list, android.R.layout.simple_spinner_item);
        meeting_type.setAdapter(meetingAdapter, 0);
        meeting_type.setOnSpinnerItemClickListener(spinnerListener);

        ArrayAdapter<CharSequence> firsttimeAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.yes_no, android.R.layout.simple_spinner_item);
        first_time.setAdapter(firsttimeAdapter, 0);
        first_time.setOnSpinnerItemClickListener(spinnerListener);

        ArrayAdapter<CharSequence> paymentAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.payment_list, android.R.layout.simple_spinner_item);
        payment_method.setAdapter(paymentAdapter, 0);
        payment_method.setOnSpinnerItemClickListener(spinnerListener);

        ArrayAdapter<CharSequence> interviewerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.interview_list, android.R.layout.simple_spinner_item);
        interviewer.setAdapter(interviewerAdapter, 0);
        interviewer.setOnSpinnerItemClickListener(spinnerListener);

        ArrayAdapter<CharSequence> contractAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.yes_no, android.R.layout.simple_spinner_item);
        contract_signed.setAdapter(contractAdapter, 0);
        contract_signed.setOnSpinnerItemClickListener(spinnerListener);

        meeting_type.setSelection(1);
        first_time.setSelection(0);
        contract_signed.setSelection(1);
        quote_box.setChecked(false);
        final_box.setChecked(false);
        rating.setText("0");

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String rank = rating.getText().toString().trim();

                if(!rank.isEmpty()){
                    String last_name=getArguments().getString("last_name");
                    String first_name=getArguments().getString("first_name");
                    String phone=getArguments().getString("phone");
                    String email=getArguments().getString("email");
                    String media=getArguments().getString("media");
                    String status=getArguments().getString("status");
                    String question1=getArguments().getString("question1");
                    String question2=getArguments().getString("question2");
                    ArrayList<String> question_type=getArguments().getStringArrayList("service");

                    String meeting_typeSelectedItem = meeting_type.getSelectedItem();
                    String first_timeSelectedItem = first_time.getSelectedItem();
                    String payment_methodSelectedItem = payment_method.getSelectedItem();
                    String interviewerSelectedItem = interviewer.getSelectedItem();
                    String contract_signedSelectedItem = contract_signed.getSelectedItem();

                    String quote_str = quote_price.getText().toString().trim();
                    String final_str = final_price.getText().toString().trim();
                    String rating_str = rating.getText().toString().trim();
                    String remark_str = remark.getText().toString().trim();

                    JSONObject data = new JSONObject();
                    try {
                        data.put("last_name", last_name);
                        data.put("first_name", first_name);
                        data.put("phone", phone);
                        data.put("email", email);
                        data.put("media", media);
                        data.put("status", status);
                        data.put("question1", question1);
                        data.put("question2", question2);
                        data.put("question_type", question_type);

                        data.put("meeting_type", meeting_typeSelectedItem);
                        data.put("first_time", first_timeSelectedItem);
                        data.put("payment", payment_methodSelectedItem);
                        data.put("interviewer", interviewerSelectedItem);
                        data.put("contract_sign", contract_signedSelectedItem);
                        data.put("quote_price", quote_str);
                        data.put("final_price", final_str);
                        data.put("rank", rating_str);
                        data.put("remark", remark_str);

                        data.put("quote_tax", quote_box.isChecked());
                        data.put("final_tax", final_box.isChecked());

                        HttpPost(getActivity(), URL_ADD_GUEST, data);

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else{
                    Toast.makeText(getActivity(),
                            R.string.fill_rating, Toast.LENGTH_LONG)
                            .show();
                }

            }
        });
        return view;
    }

    private void init(View view){
        meeting_type = view.findViewById(R.id.meeting_staff);
        first_time = view.findViewById(R.id.first_time_staff);
        payment_method = view.findViewById(R.id.payment_staff);
        interviewer = view.findViewById(R.id.interview_staff);
        contract_signed = view.findViewById(R.id.contract_staff);

        quote_price = view.findViewById(R.id.quote_price_staff);
        final_price = view.findViewById(R.id.final_price_staff);
        rating = view.findViewById(R.id.rating);
        remark = view.findViewById(R.id.remark);

        quote_box = view.findViewById(R.id.quote_checkbox);
        final_box = view.findViewById(R.id.final_checkbox);

    }
}
