package com.example.aaron.guestbook.GuestBook;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aaron.guestbook.R;
import com.isapanah.awesomespinner.AwesomeSpinner;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.example.aaron.guestbook.Configure.HttpPost;
import static com.example.aaron.guestbook.Configure.URL_GUEST_MODIFY;


/**
 * Created by aaron on 3/23/2018.
 */

public class ModifyGuest  extends AppCompatActivity {


    private Button btnSave;
    private EditText rank;
    private EditText final_price;
    private EditText follow_day;
    private EditText remark;
    private BetterSpinner contract_sign;
    private JSONObject data = new JSONObject();
    private DatePickerDialog.OnDateSetListener listener;
    private CheckBox final_tax;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_guest);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        // Progress dialog

        rank = findViewById(R.id.rating);
        final_price = findViewById(R.id.final_price);
        follow_day = findViewById(R.id.follow_day);
        remark = findViewById(R.id.remark);
        contract_sign = findViewById(R.id.contract_sign);
        btnSave = findViewById(R.id.btnModify);
        final_tax =  findViewById(R.id.modify_final_checkbox);

        Toolbar toolbar = findViewById(R.id.modify_guest_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        toolbar.setNavigationIcon(R.drawable.go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ArrayAdapter<CharSequence> contractAdapter = ArrayAdapter.createFromResource(this, R.array.yes_no, R.layout.spinner_item);
        contract_sign.setAdapter(contractAdapter);

        rank.setText(getIntent().getStringExtra("rank"));
        final_price.setText(getIntent().getStringExtra("final_price"));
        follow_day.setText(getIntent().getStringExtra("follow_day"));
        remark.setText(getIntent().getStringExtra("remark"));
        contract_sign.setText(getIntent().getStringExtra("contract_sign"));
        final String key = getIntent().getStringExtra("key");

        final_tax.setChecked(Boolean.parseBoolean((getIntent().getStringExtra("final_tax"))));

        final_tax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    data.put("final_tax", final_tax.isChecked());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        addTextChangeListener(rank);
        addTextChangeListener(final_price);
        addTextChangeListener(follow_day);
        addTextChangeListener(remark);
        addTextChangeListener(contract_sign);

        final Calendar myCalendar = Calendar.getInstance();
        follow_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ModifyGuest.this, listener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);
                String date_str = sdf.format(myCalendar.getTime());
                follow_day.setText(date_str);

            }

        };

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.length() == 0) {
                    Toast.makeText(getApplicationContext(),
                            R.string.no_mod, Toast.LENGTH_LONG).show();

                } else {
                    try {

                        Iterator x = data.keys();
                        JSONArray jsonArray = new JSONArray();

                        while (x.hasNext()){
                            String key = (String) x.next();
                            JSONObject object = new JSONObject();
                            object.put(key, data.get(key));
                            jsonArray.put(object);
                        }

                        JSONObject final_data = new JSONObject();
                        final_data.put("count", key);
                        final_data.put("data", jsonArray);
                        HttpPost(ModifyGuest.this, URL_GUEST_MODIFY, final_data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                     catch (IOException e) {
                        e.printStackTrace();
                    }}
            }
        });

    }



    public void addTextChangeListener(final EditText input){
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                try {
                    data.put(input.getTag().toString(), charSequence.toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

}