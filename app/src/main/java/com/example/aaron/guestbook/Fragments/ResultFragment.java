package com.example.aaron.guestbook.Fragments;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aaron.guestbook.Adapter.GuestAdapter;
import com.example.aaron.guestbook.Entity.Guest;
import com.example.aaron.guestbook.GuestBook.GuestBook;
import com.example.aaron.guestbook.R;
import com.example.aaron.guestbook.helper.SQLiteHandler;
import com.example.aaron.guestbook.helper.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.aaron.guestbook.Configure.logoutUser;

/**
 * Created by aaron on 3/16/2018.
 */

@SuppressLint("ValidFragment")
public class ResultFragment extends Fragment {

    private ArrayList<Guest> guests;
    private Context context;

    @SuppressLint("ValidFragment")
    public ResultFragment(ArrayList<Guest> guests, Context context){
        this.guests = guests;
        this.context = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_view, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView rc = view.findViewById(R.id.list);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider_larger));
        rc.addItemDecoration(itemDecorator);

        SQLiteHandler db = new SQLiteHandler(getActivity());

        HashMap<String, String> user = db.getUserDetails();
        String right = user.get("right");


        GuestAdapter adapter = new GuestAdapter(getActivity(), guests, right);
        rc.setLayoutManager(new LinearLayoutManager(context));
        rc.setHasFixedSize(true);
        rc.setAdapter(adapter);
    }

}
