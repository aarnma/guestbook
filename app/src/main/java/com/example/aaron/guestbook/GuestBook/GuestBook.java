package com.example.aaron.guestbook.GuestBook;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.aaron.guestbook.Fragments.NoticeFragment;
import com.example.aaron.guestbook.Fragments.PrintFragment;
import com.example.aaron.guestbook.Fragments.SearchFragmentForClient;
import com.example.aaron.guestbook.Fragments.SearchFragmentForStaff;
import com.example.aaron.guestbook.R;
import com.example.aaron.guestbook.helper.AaronSearch;
import com.example.aaron.guestbook.helper.SQLiteHandler;
import com.example.aaron.guestbook.helper.SessionManager;

import java.util.HashMap;

import static com.example.aaron.guestbook.Configure.logoutUser;

/**
 * Created by aaron on 3/9/2018.
 */

public class GuestBook extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private NoticeFragment notice;
    private SQLiteHandler db;
    private SessionManager session;
    private String right;
    private TextView title;
    private AaronSearch searchView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_book);

        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser(db, session, GuestBook.this);
        }

        HashMap<String, String> user = db.getUserDetails();
        String name = user.get("name");
        right = user.get("right");

        BottomNavigationView navigation = findViewById(R.id.navigation);

        if(Integer.valueOf(right) < 5){
            navigation.getMenu().removeItem(R.id.navigation_print);
        }


        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        fragmentManager = getSupportFragmentManager();

        Toolbar toolbar = findViewById(R.id.guest_book_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        toolbar.setNavigationIcon(R.drawable.go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        title = findViewById(R.id.toolbar_title);

        if(savedInstanceState == null)
        {
            notice = new NoticeFragment();
            changFragment(notice);
            title.setText(R.string.add);
        }


        searchView = findViewById(R.id.guest_searchView);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_add:
                    changFragment(notice);
                    title.setText(R.string.add);
                    break;
                case R.id.navigation_search:
                    if(Integer.valueOf(right) < 5){
                        SearchFragmentForClient searchFragmentForClient = new SearchFragmentForClient();
                        changFragment(searchFragmentForClient);
                    }
                    else{
                        SearchFragmentForStaff searchFragmentForStaff = new SearchFragmentForStaff();
                        changFragment(searchFragmentForStaff);
                    }
                    title.setText(R.string.search);
                    break;
                case R.id.navigation_print:
                    PrintFragment printFragment = new PrintFragment();
                    changFragment(printFragment);
                    title.setText(R.string.print);
                    break;
            }
            return true;
        }
    };



    private void changFragment(Fragment fragment) {
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();
        if(searchView != null && searchView.isSearchOpen()){searchView.closeSearch();}

    }

}
