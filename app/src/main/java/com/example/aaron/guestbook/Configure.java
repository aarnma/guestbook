package com.example.aaron.guestbook;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.aaron.guestbook.Entity.Guest;
import com.example.aaron.guestbook.Fragments.ResultFragment;
import com.example.aaron.guestbook.helper.AaronSearch;
import com.example.aaron.guestbook.helper.SQLiteHandler;
import com.example.aaron.guestbook.helper.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static android.app.Activity.RESULT_OK;


public class Configure {
    private static String URL = "http://35.233.163.57:5989/api/";
    public static String URL_LOGIN = URL + "login";
    public static String URL_ADD_GUEST = URL + "add_guest";
    public static String URL_SEARCH_GUEST = URL + "search_guest";
    public static String URL_PRINT_GUEST = URL + "print_guest";
    public static String URL_GUEST_HINT = URL + "guest_hint";
    public static String URL_GUEST_MODIFY = URL + "modify_guest";

    private static SweetAlertDialog pDialog;
    private static Call call;
    private static String[] suggestion_results;

    public static void logoutUser(SQLiteHandler db, SessionManager session, Activity activity) {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }


    public static void HttpPost(final Activity activity, String url, JSONObject data) throws IOException {

        showMessage(activity.getString(R.string.saving), SweetAlertDialog.PROGRESS_TYPE, activity);
        CreateRquest(url, data);
        call.enqueue(new Callback() {
                         @Override
                         public void onFailure(Call call, IOException e) {

                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     pDialog.dismiss();
                                     showMessage(activity.getString(R.string.server_down), SweetAlertDialog.ERROR_TYPE, activity);
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     try {
                                         JSONObject Jobject = new JSONObject(res);
                                         String status = Jobject.getString("status");
                                         status = status != null ? status : "500";
                                         pDialog.dismiss();

                                         switch (status) {
                                             case "200":
                                                 Toast.makeText(activity,
                                                         activity.getString(R.string.new_guest), Toast.LENGTH_LONG).show();
                                                 Intent intent = new Intent();
                                                 activity.setResult(RESULT_OK, intent);
                                                 activity.finish();
                                                 break;

                                             case "400":
                                                 showMessage(activity.getString(R.string.fail_add), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                             case "404":
                                                 showMessage(activity.getString(R.string.no_found), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                             case "500":
                                                 showMessage(activity.getString(R.string.status_null), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;
                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }

                                 }
                             });
                         }
                     }
        );


    }


    public static void HttpSearch(final Activity activity, String url, JSONObject data) {

        showMessage(activity.getString(R.string.searching), SweetAlertDialog.PROGRESS_TYPE, activity);
        CreateRquest(url, data);
        call.enqueue(new Callback() {
                         @Override
                         public void onFailure(Call call, IOException e) {

                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     pDialog.dismiss();
                                     showMessage(activity.getString(R.string.server_down), SweetAlertDialog.ERROR_TYPE, activity);
                                 }
                             });
                         }

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     try {
                                         JSONObject Jobject = new JSONObject(res);
                                         String status = Jobject.getString("status");
                                         status = status != null ? status : "500";
                                         pDialog.dismiss();

                                         switch (status) {
                                             case "200":
                                                 ResultFragment fragment = new ResultFragment(stringToJson(res), activity.getApplicationContext());
                                                 activity.getFragmentManager()
                                                         .beginTransaction()
                                                         .replace(R.id.result_container, fragment)
                                                         .commit();
                                                 break;

                                             case "400":
                                                 showMessage(activity.getString(R.string.fail_search), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                             case "404":
                                                 showMessage(activity.getString(R.string.no_found), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                             case "500":
                                                 showMessage(activity.getString(R.string.status_null), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }

                                 }
                             });
                         }
                     }
        );


    }

    public static ArrayList<Guest> stringToJson(String str) throws JSONException {

        ArrayList<Guest> search_result = new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object = Jarray.getJSONObject(i);
            Guest guest = new Guest(object.getString("date"),
                    object.getString("last_name"), object.getString("first_name"),
                    object.getString("email"),
                    object.getString("phone"), object.getString("media"),
                    object.getString("current_status"), object.getString("question1"),
                    object.getString("question2"), object.getString("question_type"),
                    object.getString("meeting_type"),
                    object.getString("first_time"), object.getString("payment"),
                    object.getString("interviewer"), object.getString("contract_sign"),
                    object.getString("quote_price"), object.getString("final_price"),
                    object.getString("rank"), object.getString("remark"),
                    object.getString("quote_tax"),
                    object.getString("final_tax"), object.getString("follow_date"),
                    object.getString("unique_key"));
            search_result.add(guest);
        }
        return search_result;
    }

    private static void showMessage(String info, int type, Context context) {
        pDialog = new SweetAlertDialog(context, type);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(info);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private static void CreateRquest(String url, JSONObject data) {
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody form = RequestBody.create(JSON, data.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        call = client.newCall(request);
    }

    public static void HttpHint(final Activity activity, String url, JSONObject data, final AaronSearch searchView) {

        CreateRquest(url, data);
        call.enqueue(new Callback() {
                         @Override
                         public void onFailure(Call call, IOException e) {}

                         @Override
                         public void onResponse(Call call, final Response response) throws IOException {

                             final String res = response.body().string();
                             activity.runOnUiThread(new Runnable() {
                                 @Override
                                 public void run() {
                                     try {
                                         JSONObject Jobject = new JSONObject(res);
                                         String status = Jobject.getString("status");
                                         status = status != null ? status : "500";
                                         switch (status) {
                                             case "200":
                                                 System.out.println(res);
                                                 addToSuggestionArray(res);
                                                 searchView.setSuggestions(suggestion_results);
                                                 break;
                                             case "500":
                                                 showMessage(activity.getString(R.string.status_null), SweetAlertDialog.ERROR_TYPE, activity);
                                                 break;

                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                 }
                             });
                         }
                     }
        );
    }
    private static void addToSuggestionArray(String str) throws JSONException {

        ArrayList<String> suggestions= new ArrayList<>();
        JSONObject Jobject = new JSONObject(str);
        JSONArray Jarray = Jobject.getJSONArray("output");

        for (int i = 0; i < Jarray.length(); i++) {
            JSONObject object  = Jarray.getJSONObject(i);
            String hint = object.getString("hint");
            if (!suggestions.contains(hint)){
                suggestions.add(hint);
            }

        }
        suggestion_results = suggestions.toArray(new String[suggestions.size()]);
    }

}
