package com.example.aaron.guestbook.GuestBook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.aaron.guestbook.Fragments.ResultFragment;
import com.example.aaron.guestbook.R;

import org.json.JSONException;
import org.json.JSONObject;

import static com.example.aaron.guestbook.Configure.HttpSearch;
import static com.example.aaron.guestbook.Configure.URL_SEARCH_GUEST;
import static com.example.aaron.guestbook.Configure.stringToJson;

/**
 * Created by aaron on 3/19/2018.
 */

public class ShowSearchResult extends AppCompatActivity {

    JSONObject search_data;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_result);

        Toolbar toolbar = findViewById(R.id.search_result_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        toolbar.setNavigationIcon(R.drawable.go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String type = getIntent().getStringExtra("type");
        if(type!=null){
            String key = getIntent().getStringExtra("key");
            search_data = new JSONObject();
            try {
                search_data.put("type", type);
                search_data.put("key", key);
                HttpSearch(this, URL_SEARCH_GUEST, search_data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            String data = getIntent().getStringExtra("data");
            try {
                ResultFragment fragment = new ResultFragment(stringToJson(data), getApplicationContext());
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.result_container, fragment)
                        .commit();
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                HttpSearch(this, URL_SEARCH_GUEST, search_data);
            }
        }
    }

}