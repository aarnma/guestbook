package com.example.aaron.guestbook.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.aaron.guestbook.GuestBook.AddGuest;
import com.example.aaron.guestbook.R;

/**
 * Created by aaron on 3/9/2018.
 */

public class NoticeFragment extends Fragment implements View.OnClickListener{

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notice, container, false);

        Button aggree = view.findViewById(R.id.btnAgree);
        Button disaggree = view.findViewById(R.id.btnDisagree);

        aggree.setOnClickListener(this);
        disaggree.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAgree:
                startActivity(new Intent(getActivity(), AddGuest.class));
                getActivity().finish();
                break;

            case R.id.btnDisagree:
                getActivity().onBackPressed();
                break;

        }
    }
}