package com.example.aaron.guestbook.Entity;

/**
 * Created by aaron on 3/16/2018.
 */

public class Guest {

    private String date;
    private String last_name;
    private String first_name;
    private String email;
    private String phone;
    private String media;
    private String current_status;
    private String question1;
    private String question2;
    private String question_type;
    private String meeting_type;
    private String first_time;
    private String payment;
    private String interviewer;
    private String contract_sign;
    private String quote_price;
    private String final_price;
    private String rank;
    private String remark;
    private String quote_tax;
    private String final_tax;
    private String follow_date;
    private String key;

    public String getDate() {
        return date;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getMedia() {
        return media;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public String getQuestion1() {
        return question1;
    }

    public String getQuestion2() {
        return question2;
    }

    public String getQuestion_type() {
        return question_type;
    }

    public String getMeeting_type() {
        return meeting_type;
    }

    public String getFirst_time() {
        return first_time;
    }

    public String getPayment() {
        return payment;
    }

    public String getInterviewer() {
        return interviewer;
    }

    public String getContract_sign() {
        return contract_sign;
    }

    public String getQuote_price() {
        return quote_price;
    }

    public String getFinal_price() {
        return final_price;
    }

    public String getRank() {
        return rank;
    }

    public String getRemark() {
        return remark;
    }

    public String getQuote_tax() {
        return quote_tax;
    }

    public String getFinal_tax() {
        return final_tax;
    }

    public String getFollow_date() {
        return follow_date;
    }

    public String getKey() {
        return key;
    }

    public Guest(String date, String last_name, String first_name, String email, String phone, String media, String current_status, String question1, String question2, String question_type, String meeting_type, String first_time, String payment, String interviewer, String contract_sign, String quote_price, String final_price, String rank, String remark, String quote_tax, String final_tax, String follow_date, String key) {
        this.date = date;
        this.last_name = last_name;
        this.first_name = first_name;
        this.email = email;
        this.phone = phone;
        this.media = media;
        this.current_status = current_status;
        this.question1 = question1;
        this.question2 = question2;
        this.question_type = question_type;
        this.meeting_type = meeting_type;
        this.first_time = first_time;
        this.payment = payment;
        this.interviewer = interviewer;
        this.contract_sign = contract_sign;
        this.quote_price = quote_price;
        this.final_price = final_price;
        this.rank = rank;

        this.remark = remark;
        this.quote_tax = quote_tax;
        this.final_tax = final_tax;
        this.follow_date = follow_date;

        this.key = key;
    }
}
