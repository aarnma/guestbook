package com.example.aaron.guestbook.GuestBook;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;

import android.support.annotation.RequiresApi;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;

import android.view.View;
import android.widget.Button;


import com.example.aaron.guestbook.R;
import com.example.aaron.guestbook.helper.SQLiteHandler;
import com.example.aaron.guestbook.helper.SessionManager;

import java.util.List;
import java.util.Locale;

import static com.example.aaron.guestbook.Configure.logoutUser;

public class MainPage extends AppCompatActivity implements View.OnClickListener{

    private SQLiteHandler db;
    private SessionManager session;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        Button english = findViewById(R.id.btnEnglish);
        Button chinese = findViewById(R.id.btnChinese);
        english.setOnClickListener(this);
        chinese.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        toolbar.setNavigationIcon(R.drawable.go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser(db, session, MainPage.this);
            }
        });

        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn()) {
            logoutUser(db, session, MainPage.this);
        }
    }

    public void changeAppLanguage(String language) {
        Locale myLocale = new Locale(language);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnChinese:
                changeAppLanguage("zh");
                Intent chinese_intent = new Intent(getApplicationContext(), GuestBook.class);
                startActivity(chinese_intent);
                break;
            case R.id.btnEnglish:
                changeAppLanguage("en");
                Intent english_intent = new Intent(getApplicationContext(), GuestBook.class);
                startActivity(english_intent);
                break;
        }

    }

}
