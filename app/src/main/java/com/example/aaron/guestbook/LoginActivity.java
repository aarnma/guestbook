package com.example.aaron.guestbook;

/**
 * Created by Programmer 1 on 2018-01-17.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.aaron.guestbook.GuestBook.MainPage;
import com.example.aaron.guestbook.helper.SQLiteHandler;
import com.example.aaron.guestbook.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.aaron.guestbook.Configure.URL_LOGIN;


public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btnLogin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainPage.class);
            startActivity(intent);
            finish();
        }


        Toolbar toolbar = findViewById(R.id.login_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user
                    checkLogin(email, password);
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });


    }

    private void checkLogin(final String name, final String password) {

        pDialog.setMessage("Logging in ...");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("name", name)
                .add("password", password)
                .build();

        OkHttpClient client = new OkHttpClient();
        // GET request
        Request request = new Request.Builder()
                .url(URL_LOGIN)
                .post(form)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback()
        {
            @Override
            public void onFailure(Call call, IOException e)
            {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        hideDialog();
                        Toast.makeText(getApplicationContext(),
                                "Cannot connect to server", Toast.LENGTH_LONG).show();

                    }
                });

                System.out.println(e);
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                hideDialog();
                final String res = response.body().string();
                String status = null;
                JSONObject Jobject = null;
                try {
                    Jobject = new JSONObject(res);
                    status = Jobject.getString("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final JSONObject finalJobject = Jobject;
                final String finalStatus = status != null ? status : "500";

                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        switch (finalStatus) {
                            case "200":
                                session.setLogin(true);
                                try {
                                    db.addUser( finalJobject.getString("name"),
                                            finalJobject.getString("right"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                // Launch main activity
                                Intent intent = new Intent(LoginActivity.this,
                                        MainPage.class);
                                startActivity(intent);
                                finish();
                                break;

                            case "400":
                                Toast.makeText(getApplicationContext(),
                                        "Wrong username or password", Toast.LENGTH_LONG).show();
                                break;
                            case "404":
                                Toast.makeText(getApplicationContext(),
                                        "User account no found, please register", Toast.LENGTH_LONG).show();
                                break;
                            case "500":
                                Toast.makeText(getApplicationContext(),
                                        "Server error, try again", Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                });
            }

        });

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}