package com.example.aaron.guestbook.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aaron.guestbook.R;
import com.isapanah.awesomespinner.AwesomeSpinner;

import java.util.ArrayList;

/**
 * Created by aaron on 3/13/2018.
 */

public class ClientFragment extends Fragment {
    private TextView last_name;
    private TextView first_name;
    private TextView phone;
    private TextView email;
    private AwesomeSpinner media;
    private AwesomeSpinner status;
    private TextView question1;
    private TextView question2;

    private CheckBox spouse_sponsorship;
    private CheckBox parent_sponsorship;
    private CheckBox child_sponsorship;
    private CheckBox business;
    private CheckBox work_permit;
    private CheckBox study_permit;
    private CheckBox skilled_worker;
    private CheckBox appeal;
    private CheckBox others;
    private EditText others_content;

    private ArrayList<CheckBox> checkBoxes;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_client, container,false);
        init(view);
        Button save = view.findViewById(R.id.btnSaveClient);

        checkBoxes = new ArrayList<>();
        checkBoxes.add(spouse_sponsorship);
        checkBoxes.add(parent_sponsorship);
        checkBoxes.add(child_sponsorship);
        checkBoxes.add(business);
        checkBoxes.add(work_permit);
        checkBoxes.add(study_permit);
        checkBoxes.add(skilled_worker);
        checkBoxes.add(appeal);
        checkBoxes.add(others);

        AwesomeSpinner.onSpinnerItemClickListener spinnerListener = new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int var1, String var2) {

            }
        };

        ArrayAdapter<CharSequence> mediaAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.media_list, R.layout.spinner_item);

        media.setAdapter(mediaAdapter, 0);
        media.setOnSpinnerItemClickListener(spinnerListener);

        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.status_list, R.layout.spinner_item);
        status.setAdapter(statusAdapter, 0);
        status.setOnSpinnerItemClickListener(spinnerListener);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String last_name_str = last_name.getText().toString().trim();
                String first_name_str = first_name.getText().toString().trim();
                String phone_str = phone.getText().toString().trim();

                if(!last_name_str.isEmpty() && !first_name_str.isEmpty() && !phone_str.isEmpty()){
                    String email_str = email.getText().toString().trim();
                    String media_str = media.getSelectedItem();
                    String status_str = status.getSelectedItem();
                    String question1_str = question1.getText().toString().trim();
                    String question2_str = question2 .getText().toString().trim();

                    ArrayList<String> services = new ArrayList<>();
                    for(CheckBox box : checkBoxes){
                       if(box.isChecked()){
                           if(box.getText().equals("Others")){
                               services.add("others: " + others_content.getText().toString());
                           }

                           else{services.add(box.getText().toString());}
                       }
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString("last_name",last_name_str);
                    bundle.putString("first_name",first_name_str);
                    bundle.putString("phone",phone_str);
                    bundle.putString("email",email_str);
                    bundle.putString("media",media_str);
                    bundle.putString("status",status_str);
                    bundle.putString("question1",question1_str);
                    bundle.putString("question2",question2_str);
                    bundle.putStringArrayList("service",services);


                    if (getActivity() instanceof ClientFragmentListener)
                    {
                        ((ClientFragmentListener) getActivity()).goToStaff(bundle);
                    }

                }
                else{
                    Toast.makeText(getActivity(),
                            R.string.all_field, Toast.LENGTH_LONG)
                            .show();
                }
            }
        });


        return view;
    }

    private void init(View view){
        last_name = view.findViewById(R.id.last_name_client);
        first_name = view.findViewById(R.id.first_name_client);
        phone = view.findViewById(R.id.phone_client);
        email = view.findViewById(R.id.email_client);
        media = view.findViewById(R.id.media_client);
        status = view.findViewById(R.id.status_client);
        question1 = view.findViewById(R.id.q1_client);
        question2 = view.findViewById(R.id.q2_client);

        spouse_sponsorship = view.findViewById(R.id.spouse_sponsorship_checkbox);
        parent_sponsorship = view.findViewById(R.id.parent_sponsorship_checkbox);
        child_sponsorship = view.findViewById(R.id.child_sponsorship_checkbox);
        business = view.findViewById(R.id.business_checkbox);
        work_permit = view.findViewById(R.id.work_permit_checkbox);
        study_permit = view.findViewById(R.id.study_permit_checkbox);
        skilled_worker = view.findViewById(R.id.skilled_worker_checkbox);
        appeal = view.findViewById(R.id.appeal_checkbox);
        others = view.findViewById(R.id.others_checkbox);
        others_content = view.findViewById(R.id.others_content);
    }

    public interface ClientFragmentListener
    {
        void goToStaff(Bundle bundle);
    }
}