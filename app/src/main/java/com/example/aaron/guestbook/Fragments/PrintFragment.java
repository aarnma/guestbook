package com.example.aaron.guestbook.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.example.aaron.guestbook.GuestBook.AddGuest;
import com.example.aaron.guestbook.GuestBook.ShowSearchResult;
import com.example.aaron.guestbook.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WriteException;
import me.zhouzhuo.zzexcelcreator.ZzExcelCreator;
import me.zhouzhuo.zzexcelcreator.ZzFormatCreator;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.example.aaron.guestbook.Configure.URL_PRINT_GUEST;
import static com.example.aaron.guestbook.Configure.hideKeyboard;
/**
 * Created by aaron on 3/20/2018.
 */

public class PrintFragment extends Fragment implements View.OnClickListener {
    private String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/GuestBook/";
    private String filename;
    private EditText start_date;
    private EditText end_date;
    private ProgressDialog pDialog;
    private Date start, end;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener from_date;
    private DatePickerDialog.OnDateSetListener to_date;
    private CheckBox excel;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_print, container, false);

        start_date = view.findViewById(R.id.print_from);
        end_date = view.findViewById(R.id.print_to);
        excel =  view.findViewById(R.id.print_guest_excel);
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);


        myCalendar = Calendar.getInstance();
        from_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);
                String date_str = sdf.format(myCalendar.getTime());
                try {
                    start = sdf.parse(date_str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                start_date.setText(date_str);
            }

        };

        to_date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "yyyy-MM-dd";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);
                String date_str = sdf.format(myCalendar.getTime());
                try {
                    end = sdf.parse(date_str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                end_date.setText(date_str);
            }

        };

        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);

        Button print = view.findViewById(R.id.btnPrint);
        print.setOnClickListener(this);


        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
            }

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }



        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.print_from:
                hideKeyboard(getActivity());
                new DatePickerDialog(getActivity(), from_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.print_to:
                hideKeyboard(getActivity());
                new DatePickerDialog(getActivity(), to_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnPrint:

                if (start == null || end == null) {
                    Toast.makeText(getActivity(),
                            "Please fill up start and end date!", Toast.LENGTH_LONG).show();
                } else if (!start.after(end)) {
                    getInMailList(start_date.getText().toString(), end_date.getText().toString(), URL_PRINT_GUEST);
                } else {
                    Toast.makeText(getActivity(),
                            "Start date must before end date", Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void getInMailList(final String from, final String to, final String url) {

        pDialog.setMessage("Getting data from database");
        showDialog();

        RequestBody form = new FormBody.Builder()
                .add("from", from)
                .add("to", to)
                .build();

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .post(form)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideDialog();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideDialog();
                        Toast.makeText(getActivity(),
                                "Search server disconnected", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String res = response.body().string();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject Jobject = new JSONObject(res);
                            String status = Jobject.getString("status");
                            status = status != null ? status : "500";
                            pDialog.dismiss();

                            switch (status) {
                                case "200":
                                    try {
                                        Intent intent = new Intent(getActivity(), ShowSearchResult.class);
                                        intent.putExtra("data", res);
                                        startActivity(intent);
                                        if(excel.isChecked()){
                                            pDialog.setMessage("Generating excel...");
                                            print(from, to, res);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case "404":
                                    hideDialog();
                                    Toast.makeText(getActivity(),
                                            "No Guest found", Toast.LENGTH_LONG).show();
                                    break;

                                case "500":
                                    hideDialog();
                                    Toast.makeText(getActivity(),
                                            "Server is busy", Toast.LENGTH_LONG).show();
                                    break;
                                default:
                                    hideDialog();
                                    Toast.makeText(getActivity(),
                                            "Server is disconnected", Toast.LENGTH_LONG).show();
                                    break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    public void print(String start, String end, String data) throws JSONException {

        createExcel(start, end);

        JSONObject Jobject = new JSONObject(data);
        JSONArray Jarray = Jobject.getJSONArray("output");
        String[][] array = new String[Jarray.length()+1][23];

        array[0][0] = "Date";
        array[0][1] = "Last Name";
        array[0][2] = "First Name";
        array[0][3] = "Rank";
        array[0][4] = "Phone";
        array[0][5] = "Media";
        array[0][6] = "Current Status";
        array[0][7] = "Question 1";
        array[0][8] = "Question 2";
        array[0][9] = "Question Type";
        array[0][10] = "Meeting Type";
        array[0][11] = "First Time";
        array[0][12] = "Payment";
        array[0][13] = "Interviewer";
        array[0][14] = "Contract Sign";
        array[0][15] = "Quote Price";
        array[0][16] = "Quote Tax";
        array[0][17] = "Final Price";
        array[0][18] = "Final Tax";
        array[0][19] = "Follow Date";
        array[0][20] = "Email";
        array[0][21] = "Remark";
        array[0][22] = "Key";


        for (int i=1; i <= Jarray.length(); i++) {
            JSONObject object = Jarray.getJSONObject(i-1);
            array[i][0] = object.getString("date");
            array[i][1] = object.getString("last_name");
            array[i][2] = object.getString("first_name");
            array[i][3] = object.getString("rank");
            array[i][4] = object.getString("phone");
            array[i][5] = object.getString("media");
            array[i][6] = object.getString("current_status");
            array[i][7] = object.getString("question1");
            array[i][8] = object.getString("question2");
            array[i][9] = object.getString("question_type");
            array[i][10] = object.getString("meeting_type");
            array[i][11] = object.getString("first_time");
            array[i][12] = object.getString("payment");
            array[i][13] = object.getString("interviewer");
            array[i][14] = object.getString("contract_sign");
            array[i][15] = object.getString("quote_price");
            array[i][16] = object.getString("quote_tax");
            array[i][17] = object.getString("final_price");
            array[i][18] = object.getString("final_tax");
            array[i][19] = object.getString("follow_date");
            array[i][20] = object.getString("email");
            array[i][21] = object.getString("remark");
            array[i][22] = object.getString("unique_key");
        }
        insertToExcel(array);

    }

    @SuppressLint("StaticFieldLeak")
    private void createExcel(String start, String end) {
        filename = start + "_to_" + end + "GuestList";
        String sheetName = "Sheet1";

        new AsyncTask<String, Void, Integer>() {

            @Override
            protected Integer doInBackground(String... params) {
                try {
                    ZzExcelCreator
                            .getInstance()
                            .createExcel(PATH, params[0])
                            .createSheet(params[1])
                            .close();
                    return 1;
                } catch (IOException | WriteException e) {
                    e.printStackTrace();
                    return 0;
                }
            }

            @Override
            protected void onPostExecute(Integer aVoid) {
                super.onPostExecute(aVoid);
                if (aVoid == 1) {
                } else {
                    Toast.makeText(getActivity(), "Excel表格创建失败！", Toast.LENGTH_SHORT).show();
                    hideDialog();
                }
            }
        }.execute(filename, sheetName);
    }

    @SuppressLint("StaticFieldLeak")
    private void insertToExcel(final String[][] array){

        new AsyncTask<String, Void, Integer>() {

            @Override
            protected Integer doInBackground(String... params) {
                try {
                    WritableCellFormat format = ZzFormatCreator
                            .getInstance()
                            .createCellFont(WritableFont.ARIAL)
                            .setAlignment(Alignment.CENTRE, VerticalAlignment.CENTRE)
                            .setFontSize(14)
                            .setFontColor(Colour.DARK_GREEN)
                            .getCellFormat();


                    ZzExcelCreator
                            .getInstance()
                            .openExcel(new File(PATH + filename + ".xls"))
                            .openSheet(0);

                    for(int row = 0; row < array.length; row++){
                        for(int col = 0; col < array[0].length; col++){
                            ZzExcelCreator.getInstance().setColumnWidth(col, 25)
                                    .setRowHeight(row, 400)
                                    .fillContent(col, row, array[row][col], format);

                        }
                    }

                    ZzExcelCreator.getInstance().close();

                    return 1;
                } catch (IOException | WriteException | BiffException e) {
                    e.printStackTrace();
                    return 0;
                }
            }

            @Override
            protected void onPostExecute(Integer aVoid) {
                super.onPostExecute(aVoid);
                if (aVoid == 1) {
                    Toast.makeText(getActivity(), "Excel数据导入成功！", Toast.LENGTH_SHORT).show();
                    openExcel();
                } else {
                    Toast.makeText(getActivity(), "Excel数据导入失败！", Toast.LENGTH_SHORT).show();
                }
                hideDialog();
            }
        }.execute();
    }

    private void openExcel(){
        File file = new File(PATH + filename + ".xls");
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setDataAndType(Uri.parse(file.toString()),"application/vnd.ms-excel");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(),"No Application available to viewExcel", Toast.LENGTH_SHORT).show();
        }
    }

}