package com.example.aaron.guestbook.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;
import com.example.aaron.guestbook.Entity.Guest;
import com.example.aaron.guestbook.GuestBook.ModifyGuest;
import com.example.aaron.guestbook.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aaron on 3/16/2018.
 */

public class GuestAdapter extends RecyclerView.Adapter<GuestAdapter.GuestHolder> {

    private Activity activity;
    private List<Guest> guests;
    private String right;

    public GuestAdapter(Activity activity, ArrayList<Guest> guests, String right) {
        this.activity = activity;
        this.guests = guests;
        this.right = right;

    }

    @Override
    public GuestAdapter.GuestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GuestAdapter.GuestHolder(this, activity, LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_result, parent, false), right);
    }

    @Override
    public void onBindViewHolder(final GuestAdapter.GuestHolder holder, int position) {
        Guest guest = guests.get(position);

        holder.getDate().setText(guest.getDate());
        holder.getLast_name().setText(guest.getLast_name());
        holder.getFirst_name().setText(guest.getFirst_name());
        holder.getEmail().setText(guest.getEmail());
        holder.getPhone().setText(guest.getPhone());
        holder.getFirst_time().setText(guest.getFirst_time());
        holder.getMedia().setText(guest.getMedia());
        holder.getCurrent_status().setText(guest.getCurrent_status());
        holder.getQuestion1().setText(guest.getQuestion1());
        holder.getQuestion2().setText(guest.getQuestion2());
        holder.getQuestion_type().setText(guest.getQuestion_type());
        holder.getMeeting_type().setText(guest.getMeeting_type());
        holder.getPayment().setText(guest.getPayment());
        holder.getInterviewer().setText(guest.getInterviewer());
        holder.getContract_sign().setText(guest.getContract_sign());
        holder.getQuote_price().setText(guest.getQuote_price());
        holder.getFinal_price().setText(guest.getFinal_price());
        holder.getRank().setText(guest.getRank());
        holder.getRemark().setText(guest.getRemark());
        holder.getFollow_date().setText(guest.getFollow_date());

        if (guest.getQuote_tax().equals("true")) {
            holder.getQuote_tax().setText("(" + activity.getResources().getString(R.string.tax_true) + ")");
        } else if (guest.getQuote_tax().equals("false")) {
            holder.getQuote_tax().setText("(" + activity.getResources().getString(R.string.tax_false) + ")");
        }

        if (guest.getFinal_tax().equals("true")) {
            holder.getFinal_tax().setText("(" + activity.getResources().getString(R.string.tax_true) + ")");
        } else if (guest.getFinal_tax().equals("false")) {
            holder.getFinal_tax().setText("(" + activity.getResources().getString(R.string.tax_false) + ")");
        }

    }

    @Override
    public int getItemCount() {
        return guests.size();
    }


    public class GuestHolder extends RecyclerView.ViewHolder{

        protected TextView date;
        protected TextView last_name;
        protected TextView first_name;
        protected TextView email;
        protected TextView phone;
        protected TextView first_time;
        protected TextView media;
        protected TextView current_status;
        protected TextView question1;
        protected TextView question2;
        protected TextView question_type;
        protected TextView meeting_type;
        protected TextView payment;
        protected TextView interviewer;
        protected TextView contract_sign;
        protected TextView quote_price;
        protected TextView final_price;
        protected TextView rank;
        protected TextView remark;
        protected TextView quote_tax;
        protected TextView final_tax;
        protected TextView follow_date;

        protected Activity activity;

        public GuestHolder(GuestAdapter guestAdapter, final Activity activity, View itemView, final String right) {
            super(itemView);

            this.date = itemView.findViewById(R.id.guest_date_content);
            this.last_name = itemView.findViewById(R.id.guest_last_name_content);
            this.first_name = itemView.findViewById(R.id.guest_first_name_content);
            this.email = itemView.findViewById(R.id.guest_email_content);
            this.phone = itemView.findViewById(R.id.guest_phone_content);
            this.first_time = itemView.findViewById(R.id.guest_first_time_content);
            this.media = itemView.findViewById(R.id.guest_media_type_content);
            this.current_status = itemView.findViewById(R.id.guest_current_guest_content);
            this.question1 = itemView.findViewById(R.id.guest_question_1_content);
            this.question2 = itemView.findViewById(R.id.guest_question_2_content);
            this.question_type = itemView.findViewById(R.id.guest_question_type_content);
            this.meeting_type = itemView.findViewById(R.id.guest_meeting_type_content);
            this.payment = itemView.findViewById(R.id.guest_payment_content);
            this.interviewer = itemView.findViewById(R.id.guest_interviewer_content);
            this.contract_sign = itemView.findViewById(R.id.guest_contract_sign_content);
            this.quote_price = itemView.findViewById(R.id.guest_quote_price_content);
            this.final_price = itemView.findViewById(R.id.guest_final_price_content);
            this.rank = itemView.findViewById(R.id.guest_rank_content);
            this.remark = itemView.findViewById(R.id.guest_remark_content);
            this.quote_tax = itemView.findViewById(R.id.guest_quote_tax);
            this.final_tax = itemView.findViewById(R.id.guest_final_tax);
            this.follow_date = itemView.findViewById(R.id.guest_follow_day_content);

            this.activity = activity;
            if(Integer.valueOf(right) > 4){
                ImageView arrow = itemView.findViewById(R.id.guest_arrow_icon);
                arrow.setVisibility(View.VISIBLE);
                arrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        initBottomSheet(right);
                    }});
            }



        }

        private void initBottomSheet(String right){
            BottomSheet.Builder btsheet = new BottomSheet.Builder(activity);
            String name = last_name.getText() + " " + first_name.getText();
            final String rank_str = rank.getText().toString();
            final String final_price_str = final_price.getText().toString();
            final String follow_day_str = follow_date.getText().toString();
            final String remark_str = remark.getText().toString();
            final String contract_sign_str = contract_sign.getText().toString();

            btsheet.sheet(R.menu.bottom_menu);
            btsheet.title(activity.getString(R.string.opearting) + " " + name)
                    .listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case R.id.modify:

                                    Intent intent = new Intent(activity, ModifyGuest.class);
                                    intent.putExtra("rank", rank_str);
                                    intent.putExtra("final_price", final_price_str);
                                    intent.putExtra("follow_day", follow_day_str);
                                    intent.putExtra("remark", remark_str);
                                    intent.putExtra("contract_sign", contract_sign_str);
                                    intent.putExtra("key", guests.get(getPosition()).getKey());

                                    switch (final_tax.getText().toString()){
                                        case "(Tax Included)":
                                            intent.putExtra("final_tax", "true");
                                            break;

                                        case "(Tax Not Included)":
                                            intent.putExtra("final_tax", "false");
                                            break;

                                        case "(含税)":
                                            intent.putExtra("final_tax", "true");
                                            break;

                                        case "(不含税)":
                                            intent.putExtra("final_tax", "false");
                                            break;

                                    }

                                    activity.startActivityForResult(intent,1);

                                    break;
                            }
                        }
                    }).show();

        }

        public TextView getDate() {
            return date;
        }

        public TextView getLast_name() {
            return last_name;
        }

        public TextView getFirst_name() {
            return first_name;
        }

        public TextView getEmail() {
            return email;
        }

        public TextView getPhone() {
            return phone;
        }

        public TextView getFirst_time() {
            return first_time;
        }

        public TextView getMedia() {
            return media;
        }

        public TextView getCurrent_status() {
            return current_status;
        }

        public TextView getQuestion1() {
            return question1;
        }

        public TextView getQuestion2() {
            return question2;
        }

        public TextView getQuestion_type() {
            return question_type;
        }

        public TextView getMeeting_type() {
            return meeting_type;
        }

        public TextView getPayment() {
            return payment;
        }

        public TextView getInterviewer() {
            return interviewer;
        }

        public TextView getContract_sign() {
            return contract_sign;
        }

        public TextView getQuote_price() {
            return quote_price;
        }

        public TextView getFinal_price() {
            return final_price;
        }

        public TextView getRank() {
            return rank;
        }

        public TextView getRemark() {
            return remark;
        }

        public TextView getQuote_tax() {
            return quote_tax;
        }

        public TextView getFinal_tax() {
            return final_tax;
        }

        public TextView getFollow_date() {
            return follow_date;
        }

        public Activity getActivity() {
            return activity;
        }


    }

}
