package com.example.aaron.guestbook.GuestBook;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import com.example.aaron.guestbook.Fragments.ClientFragment;
import com.example.aaron.guestbook.Fragments.StaffFragment;
import com.example.aaron.guestbook.R;


/**
 * Created by aaron on 3/12/2018.
 */

public class AddGuest extends AppCompatActivity implements ClientFragment.ClientFragmentListener {

    private ClientFragment client;
    private StaffFragment staff;
    private String current_frag = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_guest);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if(savedInstanceState == null)
        {
            Toolbar toolbar = findViewById(R.id.add_client_toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setTitle("");
            toolbar.setSubtitle("");
            toolbar.setNavigationIcon(R.drawable.go_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            client = new ClientFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, client)
                    .commit();
            current_frag = "client";
        }

    }

    @Override
    public void goToStaff(Bundle bundle)
    {

        Fragment fragment = getFragmentManager().findFragmentByTag("staff");
        if (fragment == null) {
            staff = new StaffFragment();
            staff.setArguments(bundle);
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, staff, "staff")
                    .addToBackStack(null)
                    .commit();
        }
        else{
            getFragmentManager()
                    .beginTransaction()
                    .show(staff)
                    .commit();
        }
        current_frag = "staff";
        getFragmentManager()
                .beginTransaction().hide(client).commit();

    }


    @Override
    public void onBackPressed() {
        if(current_frag.equals("staff")){
            getFragmentManager()
                    .beginTransaction().hide(staff).commit();
            getFragmentManager()
                    .beginTransaction().show(client).commit();
            current_frag = "client";
        }
        else{
            this.finish();
        }
    }
}